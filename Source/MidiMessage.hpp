//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Scott Ballard on 05/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>
/** class for information about a midi note */
class MidiMessage
{
    
public:
    /**constructor */
    MidiMessage();
    
    /**destructor */
    ~MidiMessage();
    
    /** mutator for setting note number of chosen note */
    void setNoteNumber(int newNoteNumber);
    
    /** mutator for setting note velocity of chosen note */
    void setVelocity(int velValue);
    
    /** mutator for setting chosen channel number */
    void setChannel (int chanNum);
    
    //constant values
    /** Accessor for getting note number */
    int getNoteNumber() const;
    /** Accessor for getting velocity */
    int getVelocityNumber() const;
    /** Accessor for getting channel number */
    int getChannelNumber() const;
    
    /** convert midi note into frequency in hertz */
    float getMidiNoteInHertz() const;
    /** convert velocity into a scaling value */
    float getFloatVelocity() const;
    
    
private:
    int notenumber;
    int noteVel;
    int channelNo;
    

    
};
#endif /* MidiMessage_hpp */
