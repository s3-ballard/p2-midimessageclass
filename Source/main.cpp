//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"

int main (int argc, const char* argv[])
{
    
    std:: cout << "test\n ";
    //declaration of object
    MidiMessage note;
    int userNote;
    int velocitynumber;
    int channelnumber;

    // insert code here...

    //print out to console the existing note num and freq
    std:: cout << "Note Number " << note.getNoteNumber() << std::endl;
    std:: cout << "Note Frequency " << note.getMidiNoteInHertz() << std::endl;
    std:: cout << "Velocity int " << note.getVelocityNumber() << std::endl;
    std:: cout << "Velocity float " << note.getFloatVelocity() << std::endl;
    std:: cout << "Channel Number " << note.getChannelNumber() << std::endl;
    
    std:: cout << "enter in a new note number" << std::endl;
    //user enters in new note number
    std::cin >> userNote;
    

    //call to object function to store a new note number
    note.setNoteNumber(userNote);
    
    std:: cout << "enter in a new velocity number" << std::endl;
    std::cin >> velocitynumber;
    note.setVelocity(velocitynumber);
    
    std:: cout << "enter in a new channel number" << std::endl;
    std::cin >> channelnumber;
    note.setChannel(channelnumber);
    
    //print out new number and freq
    std:: cout << "New Note Number " << note.getNoteNumber() << std::endl;
    std:: cout << "New Note Frequency " << note.getMidiNoteInHertz() << std::endl;
    std:: cout << "New Velocity int " << note.getVelocityNumber() << std::endl;
    std:: cout << "New Velocity float " << note.getFloatVelocity() << std::endl;
    std:: cout << "New Channel Number " << note.getChannelNumber() << std::endl;

    
    //end
    return 0;
}

