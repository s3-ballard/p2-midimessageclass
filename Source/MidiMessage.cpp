//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Scott Ballard on 05/10/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <cmath>

MidiMessage::MidiMessage()
{
    notenumber = 60;
    noteVel = 63;
    channelNo = 1;
}
MidiMessage::~MidiMessage()
{
    
}
void MidiMessage::setNoteNumber(int newNoteNumber)
{
    if (newNoteNumber > 127.0){
        notenumber = 127.0;
    }
    else if (newNoteNumber < 0.0){
        notenumber = 0.0;
    }
    else{
       notenumber = newNoteNumber;
    }
   
}
void MidiMessage::setVelocity(int velValue)
{
    if (velValue > 127.0){
        noteVel = 127.0;
    }
    else if (velValue < 0){
        noteVel = 0.0;
    }
    else {
       noteVel = velValue;
    }
    
    
}
void MidiMessage::setChannel (int chanNum)
{
    if (chanNum > 16){
        channelNo = 16;
    }
    else if (chanNum < 1){
        channelNo = 1;
    }
    else{
       channelNo = chanNum;
    }
    
}

int MidiMessage::getNoteNumber() const
{
    return notenumber;
}
int MidiMessage::getVelocityNumber() const
{
    return noteVel;
}
int MidiMessage::getChannelNumber() const
{
    return channelNo;
}

float MidiMessage::getMidiNoteInHertz() const
{
    return 440 * pow(2, (notenumber-69) / 12.0);
}

float MidiMessage::getFloatVelocity() const
{
    return noteVel / 127.0;
}




